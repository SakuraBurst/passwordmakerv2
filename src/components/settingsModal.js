import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button, FormCheck } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import {
  changeCapLetters,
  changeSmallLetters,
  changeNumbers,
  changeSymbols
} from '../redux/actions/passwordOptions';

export default function SettingsModal({ show, onHide }) {
  const dispatch = useDispatch();
  const options = useSelector(state => state.passwordOptions);
  return (
    <Modal
      onHide={onHide}
      show={show}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">Options</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <h4>Pls select password options</h4>
        <div className="modalCheckBoxDiv">
          <FormCheck
            checked={options.capitalLetters}
            id="capitalLettersOption"
            onChange={() => dispatch(changeCapLetters())}
            label="Capital Letters"
          />
          <FormCheck
            checked={options.smallLetters}
            id="smallLetterOption"
            onChange={() => dispatch(changeSmallLetters())}
            label="Small letters"
          />
          <FormCheck
            checked={options.numbers}
            id="numbersOption"
            onChange={() => dispatch(changeNumbers())}
            label="Numbers"
          />
          <FormCheck
            checked={options.symbols}
            id="symbolsOption"
            onChange={() => dispatch(changeSymbols())}
            label="Symbols"
          />
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="success" onClick={onHide}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

SettingsModal.propTypes = {
  show: PropTypes.bool.isRequired,
  onHide: PropTypes.func.isRequired
};
