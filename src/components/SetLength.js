import React, { useCallback } from 'react';
import { Slider } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import lengthSeter from '../redux/actions/setLength';

function SetLength() {
  const dispatch = useDispatch();
  const length = useSelector(state => state.passwordLength);
  const setLength = useCallback(
    l => {
      dispatch(lengthSeter(l));
    },
    [length]
  );
  return (
    <div style={{ width: '100%' }}>
      <Slider
        value={length}
        aria-labelledby="discrete-slider"
        valueLabelDisplay="on"
        step={1}
        marks
        min={5}
        max={20}
        onChange={(e, v) => {
          setLength(v);
        }}
      />
    </div>
  );
}

export default SetLength;
