/* eslint-disable no-nested-ternary */
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button } from 'react-bootstrap';
import SetLength from './SetLength';
import NewPassword from './NewPassword';
import makePassword from '../redux/actions/makePassword';
import Settings from './settings';
import PreviousPassword from './previousPassword';

function MainWindow() {
  const dispatch = useDispatch();
  const length = useSelector(state => state.passwordLength);
  const options = useSelector(state => state.passwordOptions);
  function generate() {
    dispatch(makePassword(length, options));
  }
  return (
    <div id="mainWindow">
      <SetLength />
      <Button variant="dark" onClick={generate} style={{ width: '140px' }}>
        Generate
      </Button>
      <NewPassword />
      <Settings />
      <PreviousPassword />
    </div>
  );
}

export default MainWindow;
