import React, { useState, useCallback } from 'react';
import { Button } from 'react-bootstrap';
import { useSelector } from 'react-redux';

export default function PreviousPassword() {
  const [showPassword, changePasswordShow] = useState(false);
  const prevPass = useSelector(state => state.previosPassword);
  const changePasswordVisability = useCallback(() =>
    changePasswordShow(!showPassword)
  );
  return (
    <div className="prevPass">
      <Button variant="info" onClick={changePasswordVisability}>
        Show Previous Password
      </Button>
      {showPassword && prevPass.length > 0 ? (
        <h4 style={{ marginTop: 25 }}>{prevPass}</h4>
      ) : (
        <div />
      )}
    </div>
  );
}
