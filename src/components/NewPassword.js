import React from 'react';
import { useSelector } from 'react-redux';

function NewPassword() {
  const password = useSelector(state => state.newPassword);
  return (
    <div style={{ marginTop: '20px' }}>
      <h2>{password}</h2>
    </div>
  );
}
export default NewPassword;
