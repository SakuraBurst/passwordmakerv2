import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import SettingsModal from './settingsModal';

export default function Settings() {
  const [modalShow, setModalShow] = useState(false);
  return (
    <>
      <Button variant="secondary" onClick={() => setModalShow(true)}>
        Settings
      </Button>

      <SettingsModal show={modalShow} onHide={() => setModalShow(false)} />
    </>
  );
}
