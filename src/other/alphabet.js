export default {
  capitalLetters: 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ',
  smallLetters: 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz',
  numbers: '1234567890123456789012345678901234567890',
  symbols: '_!@#$%^&*()|\\/_!@#$%^&*()|\\/_!@#$%^&*()|\\/'
};
