/* eslint-disable consistent-return */
/* eslint-disable no-plusplus */
import alphabet from './other/alphabet';

export default function generatePassword(length, options) {
  const keys = Object.keys(alphabet);
  const alphabetKeys = keys.filter(a => options[a]);
  // пока оставлю здесь, потом может перекину в отдельный файл
  const regExp = new RegExp(
    `${alphabetKeys.indexOf('capitalLetters') > -1 ? '(?=.*[A-Z])' : ''}${
      alphabetKeys.indexOf('smallLetters') > -1 ? '(?=.*[a-z])' : ''
    }${alphabetKeys.indexOf('numbers') > -1 ? '(?=.*[\\d])' : ''}${
      alphabetKeys.indexOf('symbols') > -1 ? '(?=.*[\\W])' : ''
    }`
  );
  let passwordGen = '';
  let passwordPass = false;
  function generate() {
    for (let i = 0; i < length; i++) {
      const randomType = Math.floor(Math.random() * alphabetKeys.length);
      passwordGen +=
        alphabet[alphabetKeys[randomType]][
          Math.floor(Math.random() * alphabetKeys[randomType].length)
        ];
    }
  }

  generate();
  passwordPass = regExp.test(passwordGen);
  while (!passwordPass) {
    passwordGen = '';
    generate();
    passwordPass = regExp.test(passwordGen);
  }
  if (passwordPass) {
    return passwordGen;
  }
}
