export const SETLENGTH = 'SETLENGTH';

const lengthSeter = length => {
  return {
    type: SETLENGTH,
    payload: length
  };
};

export default lengthSeter;
