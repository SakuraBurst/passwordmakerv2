import generatePassword from '../../makePassword';

export const PASSWORD = 'PASSWORD';

const makePassword = (length, options) => {
  const password = generatePassword(length, options);
  return {
    type: PASSWORD,
    payload: password
  };
};

export default makePassword;
