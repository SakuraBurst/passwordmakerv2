export const CAPPITAL_LETTERS = 'CAPPITAL_LETTERS';
export const SMALL_LETTERS = 'SMALL_LETTERS';
export const NUMBERS = 'NUMBERS';
export const SYMBOLS = 'SYMBOLS';

export const changeCapLetters = () => {
  return {
    type: CAPPITAL_LETTERS
  };
};

export const changeSmallLetters = () => {
  return {
    type: SMALL_LETTERS
  };
};

export const changeNumbers = () => {
  return {
    type: NUMBERS
  };
};

export const changeSymbols = () => {
  return {
    type: SYMBOLS
  };
};
