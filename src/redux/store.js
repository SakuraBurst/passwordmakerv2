/* eslint-disable import/no-extraneous-dependencies */
import { createStore } from 'redux';
import { devToolsEnhancer } from 'redux-devtools-extension';
import mainReducer from './reducers/mainReducer';

const store = createStore(mainReducer, devToolsEnhancer());

export default store;
