import { SETLENGTH } from '../actions/setLength';
import { PASSWORD } from '../actions/makePassword';
import {
  CAPPITAL_LETTERS,
  SMALL_LETTERS,
  NUMBERS,
  SYMBOLS
} from '../actions/passwordOptions';

const initialState = {
  passwordLength: 12,
  newPassword: '',
  previosPassword: '',
  passwordOptions: {
    capitalLetters: true,
    smallLetters: true,
    numbers: true,
    symbols: true
  }
};

const mainReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SETLENGTH: {
      return {
        ...state,
        passwordLength: payload
      };
    }
    case PASSWORD: {
      return {
        ...state,
        previosPassword: state.newPassword,
        newPassword: payload
      };
    }
    case CAPPITAL_LETTERS: {
      return {
        ...state,
        passwordOptions: {
          ...state.passwordOptions,
          capitalLetters: !state.passwordOptions.capitalLetters
        }
      };
    }
    case SMALL_LETTERS: {
      return {
        ...state,
        passwordOptions: {
          ...state.passwordOptions,
          smallLetters: !state.passwordOptions.smallLetters
        }
      };
    }
    case NUMBERS: {
      return {
        ...state,
        passwordOptions: {
          ...state.passwordOptions,
          numbers: !state.passwordOptions.numbers
        }
      };
    }
    case SYMBOLS: {
      return {
        ...state,
        passwordOptions: {
          ...state.passwordOptions,
          symbols: !state.passwordOptions.symbols
        }
      };
    }
    default:
      return state;
  }
};

export default mainReducer;
